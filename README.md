# Boson statistics
example {  
T=1  
number of particles = 10  
possible energy states: 0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6  
number of iterations = 1000000  
}

![alt text](example/boson_distribution_iterative.png)
![alt text](example/boson_distribution_combinatorial.png)


Logarithmic scale:  
![alt text](example/boson_distribution_iterative_logarithmic.png)
![alt text](example/boson_distribution_combinatorial_logarithmic.png)

INSTALLATION INSTRUCTIONS:

**On Linux:**
1. navigate to folder with project
2. pipenv install
3. pipenv shell
4. python main.py

Or:
1. navigate to folder with project
2. pip install matplotlib
3. pip install progressbar2
4. python main.py