import random
import sys
import progressbar
import math
import matplotlib.pyplot as plt

"""
input example:
1
10
0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6
1000000
"""

k = 1.380649
T = int(input("please input temperature of the system in K: "))
beta = 1 / (k * T)

number_of_particles = int(input("please input number of particles (under 100_000 usually fits in cache): "))

energy_levels = list(
    map(float, input("please input possible energy levels separated by spaces (in units of 10^-23 * J): ").split()))
energy_levels.sort()

number_of_iterations = int(input("please input number of iterations (at least 100 * N_of_particles recommended): "))

# randomly assigning energy level to each particle
list_of_particles = random.choices(range(len(energy_levels)), k=number_of_particles)

total_sum_of_particles_by_level = [0] * len(energy_levels)
for iteration in progressbar.progressbar(range(number_of_iterations)):

    # skip 10% of iterations before gathering statistics
    if iteration >= number_of_iterations // 10:
        for p in list_of_particles:
            total_sum_of_particles_by_level[p] += 1

    # choose random particle and direction to move it to
    particle = random.randint(0, number_of_particles - 1)
    neighbour_level = list_of_particles[particle] + random.choice([-1, 1])

    # check if chosen neighbour energy state is actually possible
    if neighbour_level < 0 or neighbour_level >= len(energy_levels):
        continue

    # Monte Carlo part
    energy_delta = energy_levels[neighbour_level] - energy_levels[list_of_particles[particle]]
    if energy_delta > 0:
        r = random.random()
        if r < math.exp(-1 * beta * energy_delta):
            list_of_particles[particle] = neighbour_level
    else:
        list_of_particles[particle] = neighbour_level

average_n_of_particles_by_level = [0] * len(energy_levels)
for (i, sum_of_particles) in enumerate(total_sum_of_particles_by_level):
    average_n_of_particles_by_level[i] = sum_of_particles / (number_of_iterations - number_of_iterations // 10)

# debug
# print(total_sum_of_particles_by_level)
# print(average_n_of_particles_by_level)
# print(sum(average_n_of_particles_by_level))

# plotting
plt.plot(energy_levels, average_n_of_particles_by_level)
plt.title("Bosons distribution by energy levels (iterative model)")
plt.xlabel("energy level (10^-23 * J)")
plt.ylabel("N of particles in energy level")
plt.yscale("log")
plt.show()


# k - recursion depth (index of energy level for current sum)
# i - energy level index for calculation of average ni (ignored for calculation of z)
def repeating_sum(n_of_particles, levels, i=-1, k=0):
    # base case
    # check if end of recursion was reached (last energy level was reached on previous step)
    if k == len(levels):
        return 1
    else:
        result = 0
        for index in range(n_of_particles + 1):
            if i == k:
                result += index * math.exp(-beta * levels[k] * index) * repeating_sum(n_of_particles - index,
                                                                                      levels, i=i, k=k + 1)
            else:
                result += math.exp(-beta * levels[k] * index) * repeating_sum(n_of_particles - index,
                                                                              levels, i=i, k=k + 1)
        return result


z = repeating_sum(number_of_particles, energy_levels)
average_n_of_particles_by_level = [0] * len(energy_levels)
for i in progressbar.progressbar(range(len(energy_levels))):
    average_n_of_particles_by_level[i] = repeating_sum(number_of_particles, energy_levels, i=i) / z

# debug
# print(z)
# print(average_n_of_particles_by_level)
# print(sum(average_n_of_particles_by_level))

plt.plot(energy_levels, average_n_of_particles_by_level)
plt.title("Bosons distribution by energy levels (combinatorial model)")
plt.xlabel("energy level (10^-23 * J)")
plt.ylabel("N of particles in energy level")
plt.yscale("log")
plt.show()
